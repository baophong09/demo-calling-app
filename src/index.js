import {createStackNavigator} from "react-navigation";
import CallingScreen from "./screens/Calling";
import CallReceiveScreen from "./screens/CallReceive";
import CallRoomScreen from "./screens/CallRoom";

const routeConfig = {
    Calling: {screen: CallingScreen},
    CallReceive: {screen: CallReceiveScreen},
    CallRoom: {screen: CallRoomScreen},
};

const stackNavigatorConfig = {
    initialRouteName: 'Calling',
    headerMode: 'none'
};

const App = createStackNavigator(routeConfig, stackNavigatorConfig);

export default App;