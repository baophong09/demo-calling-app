import axios from 'axios';
import {FLASK_SERVER_API} from "../settings";

let axiosInstance = axios.create({baseURL: FLASK_SERVER_API});

export default class FlaskAPI {
    static paramsToStr(data) {
        const ret = [];
        for (let d in data)
            ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
        return ret.join('&');
    }

    static post = async (endpoint, params) => {
        params = params || {};
        let url = `${FLASK_SERVER_API}/${endpoint}`;

        console.log(url, params);

        return new Promise((resolve, reject) => {
            axiosInstance.post(url, params)
                .then(data => resolve([null, data]))
                .catch(e => resolve([e, null]))
        });
    };

    static get = async (endpoint, params) => {
        params = params || {};
        let url = `${endpoint}?${FlaskAPI.paramsToStr(params)}`;

        return new Promise((resolve, reject) => {
            // If cache is used then try to load data from cache
            axiosInstance.get(url)
                .then(data => resolve([null, data]))
                .catch(e => resolve([e, null]))
        });
    }
}
