import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/messaging';


let _app = firebase.initializeApp({
    apiKey: "AIzaSyDuh4wtL8QIztyfJACfIHI8y1sTCN_vs0g",
    authDomain: "test-calling-83e2b.firebaseapp.com",
    databaseURL: "https://test-calling-83e2b.firebaseio.com",
    projectId: "test-calling-83e2b",
    storageBucket: "test-calling-83e2b.appspot.com",
    messagingSenderId: "741269025144"
});

export class _Firebase {
    constructor() {
        this._app = _app;
        this.timestamp = firebase.database.ServerValue.TIMESTAMP;
    }

    set = (name, value) => {
        return new Promise(resolve => {
            this._app.database().ref(name).set(value, () => resolve(true));
        })

    };

    push = (name, value) => {
        return new Promise(resolve => {
            this._app.database().ref(name).push(value, () => resolve(true));
        })
    };

    fetch = (name) => {
        return new Promise((resolve) => {
            this._app.database().ref(name).once('value').then(snapshot => {
                resolve(snapshot.val());
            })
        });
    };

    on = (callback, name, event, limitToLast=5) => {
        this._app.database().ref(name).limitToLast(limitToLast)
            .on(event, callback);
    };

    ref = (_ref) => this._app.database().ref(_ref);

    off = (name) => {
        this._app.database().ref(name).off();
    };

    onDisconnect = (name, getNewValue) => {
        this._app.database().ref(name).onDisconnect().set(getNewValue());
    };
}


export default new _Firebase();
