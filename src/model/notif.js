import {Alert, AsyncStorage} from "react-native";
import FCM, {FCMEvent} from "react-native-fcm";
import Firebase from "../services/firebase";
import {sha256} from "../services/utils";

function randomString(length) {
    var chars = '0123456789'.split('');

    if (! length) {
        length = Math.floor(Math.random() * chars.length);
    }

    var str = '';
    for (var i = 0; i < length; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
}

FCM.on(FCMEvent.Notification, async (notif) => {
    // Don't process notif here if app is already launched
    // because the notif will be processed in app instead of headless here
    if (NotifModel._isAppLaunched) return;

    NotifModel.notifHandler(true, notif);
});

export default class NotifModel {
    static _fcmToken = null;
    static _navigation = null;

    // Because Headless Notif is listened already so we need to set this flag,
    // so Headless listener will not process the incoming notif if all is launched
    static _isAppLaunched = false;

    static init = async () => {
        console.log('[NotifModel] Init the notifications');

        let userId = await AsyncStorage.getItem('userId');

        if (!userId) {
            userId = randomString(6);
            await AsyncStorage.setItem('userId', userId)
        }

        // Require permissions
        let isAllowed = NotifModel.requestPermissions();
        if (!isAllowed) return Alert.alert('Error', 'Please allow notifications');

        // Get and set token
        let fcmToken = await FCM.getFCMToken();

        if (!fcmToken) return Alert.alert('Error', 'Unable to initialize notifications');
        NotifModel._fcmToken = fcmToken;

        // $store.session.setFcmToken(fcmToken);

        let isUpdated = NotifModel.updateUserFcmToken(fcmToken);
        if (!isUpdated) return Alert.alert('Error', 'Unable to update user notification token');

        // Cancel Headless listener because App is started so we handle here
        NotifModel._isAppLaunched = true;

        // Start listen
        FCM.on(FCMEvent.Notification, notif => {
            NotifModel.notifHandler(false, notif);
        });
    };

    static requestPermissions = async () => {
        try {
            await FCM.requestPermissions();
        } catch (e) {
            console.log(e);
            return false;
        }

        return true;
    };

    static updateUserFcmToken = async (fcmToken) => {
        let userId = await AsyncStorage.getItem('userId');

        Firebase.set(`users/${userId}`, {
            _id: userId,
            token: fcmToken
        });

        return true;
    };

    static notifHandler = (isHeadless, notif) => {
        // TODO: check and remove this line : If app is already launched we ignore local notifications
        if (!isHeadless && notif._notificationType === 'will_present_notification') return;

        if (!notif.type) return;

        let notifHandler = notificationHandlers[notif.type];
        if (!notifHandler) return console.warn('Unknown notification type');

        let navigation = NotifModel._navigation;
        let isClicked = notif.opened_from_tray;

        // For CLASH_INVITE, do not show notif but call the action directly
        // so we process this exception case here
        if (notif.type === 'CALL' || notif.type === 'ANSWER') {
            // if (isHeadless) {
            //     AsyncStorage.setItem('ClashFoot:isFromHeadless', '1');
            //     AsyncStorage.setItem('CallingWhenOff:clashId', notif?.clashId);
            //     AsyncStorage.setItem('CallingWhenOff:sessionId', notif?.sessionId);
            // }

            notifHandler(navigation, notif);
            return;
        }

        // Call action if click on notif else then show notif
        if (isClicked) {
            notifHandler(navigation, notif);
        } else {
            NotifModel.showNotif(notif);
        }
        // TODO: Check this
        if (notif.finish) notif.finish();
    };

    static getNotifTitleBody = (notif) => {
        let title = 'Calling';
        let body = 'Calling';

        return {title, body};
    };

    static showNotif = (notif) => {
        let {type} = notif;
        let {title, body} = NotifModel.getNotifTitleBody(notif);

        let id = `${type}-${_.random(100000, 999999)}`;

        FCM.presentLocalNotification({
            ...notif,
            type,
            id,
            title,
            body,
            priority: "high",
            show_in_foreground: true,
            large_icon: "ic_launcher",// Android only
            icon: "ic_launcher",
            vibrate: 300, // Android only default: 300, no vibration if you pass null
            lights: true, // Android only, LED blinking (default false)
        });
    };
}

const _goToCallingScreen = async (navigation, notif) => {
    // let user = $store.user.value;
    //
    // if ("available" in user && user.available === false) {
    //     return console.log('User not available avoid show notification')
    // }
    //
    // $store.user.setAvailable(false);
    if (navigation) {
        navigation.navigate('CallReceive', {
            fromId: notif.fromId,
            fromLogin: notif?.fromLogin,
            fromImage: notif?.fromImage
        });
    }
};

const _goToCallRoomScreen = async (navigation, notif) => {
    if (navigation) {
        navigation.navigate('CallRoom', {
            sessionId: notif?.sessionId,
            token: notif?.token
        });
    }
}

const notificationHandlers = {
    "CALL": _goToCallingScreen,
    "ANSWER": _goToCallRoomScreen
};