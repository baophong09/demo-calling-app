import React from "react";
import {Text, View, TextInput, Button, AsyncStorage, Image, Modal, Alert} from "react-native";
import NotifModel from "../model/notif";
import FlaskAPI from "../services/flaskApi";

class CallingScreen extends React.Component {
    state = {
        callTo: "",
        userId: null,
        callToUser: null,
        isCalling: false,
        openLauncher: false
    };

    async componentDidMount() {
        let {navigation} = this.props;
        NotifModel._navigation = navigation;
        await NotifModel.init();
        let userId = await AsyncStorage.getItem('userId');

        this.setState({userId});
    }

    startCall = async () => {
        let {userId, callTo, isCalling} = this.state;

        if (!callTo || isCalling) return;

        this.setState({openLauncher: true, isCalling: true});

        let [error, response] = await FlaskAPI.post(`${callTo}/call`, {
            userId
        });

        if (error) {
            Alert.alert(
                `Error when call. Maybe user with id: ${callTo} not found`,
                null,
                [
                    {text: 'OK', onPress: this.cancelCall},
                ],
                {cancelable: false},
            );
            return;
        }

        let callToUser = response.data;
        this.setState({callToUser});
    }

    cancelCall = async () => {
        this.setState({
            callTo: "",
            callToUser: null,
            isCalling: false,
            openLauncher: false
        });
    }

    render() {
        let {callTo, userId, openLauncher, callToUser} = this.state;

        return (
            <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
                {openLauncher ?
                <View style={{alignItems: 'center', flex: 1, backgroundColor: '#ffffff'}}>
                    {callToUser &&
                    <View style={{alignItems: 'center', top: 50}}>
                        <Image source={{uri: callToUser.picture}} style={{width: 200, height: 200}}/>
                        <Text style={{color: 'black'}}>{callToUser.name}</Text>
                    </View>
                    }

                    <View style={{flexDirection: 'row', alignItems: 'center', top: 250}}>
                        <Button title="Cancel" onPress={this.cancelCall}/>
                    </View>
                </View>
                    :
                <>
                    <Text>Your ID: {userId}</Text>
                    <TextInput
                        style={{height: 40, borderColor: 'gray', borderWidth: 1, width: 250}}
                        onChangeText={callTo => this.setState({callTo})} value={callTo} placeholer={"Call To ..."} keyboardType={'number-pad'}/>
                    <Button title="Call" onPress={this.startCall}/>
                </>
                }
            </View>

        );
    }
}

export default CallingScreen;