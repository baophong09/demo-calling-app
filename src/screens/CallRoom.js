import React from "react";
import {View, Dimensions, BackHandler} from "react-native";
import { OTSession, OTPublisher, OTSubscriber } from 'opentok-react-native';
import {NavigationActions} from "react-navigation";


class CallRoom extends React.Component {
    state = {
        sessionId: null,
        token: null,
    };

    apiKey = '46311592';

    constructor(props) {
        super(props);

        this.params = this.props.navigation.state.params || {};
        let d = Dimensions.get('window');
        this.width = d.width;
        this.height = d.height;
    }

    componentWillMount() {
        this.setState({
            sessionId: this.params.sessionId,
            token: this.params.token
        })
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        return this.props.navigation.reset([NavigationActions.navigate({routeName: 'Calling'})], 0);
    };

    render() {
        let {sessionId, token} = this.state;

        console.log(sessionId, token);

        return (
            <View style={{alignItems: 'center', flex: 1, backgroundColor: '#ffffff'}}>
                {sessionId && token &&
                    <OTSession apiKey={this.apiKey} sessionId={sessionId} token={token}>
                        <OTPublisher style={{ width: this.width, height: this.height / 2 }} />
                        <OTSubscriber style={{ width: this.width, height: this.height / 2 }} />
                    </OTSession>
                }
            </View>
        );
    }
}

export default CallRoom;