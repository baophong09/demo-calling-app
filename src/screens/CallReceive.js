import React from "react";
import {Button, Image, Text, View, AsyncStorage} from "react-native";
import FlaskAPI from "../services/flaskApi";

class CallReceive extends React.Component {
    state = {};

    userId = null;

    constructor(props) {
        super(props);

        this.params = this.props.navigation.state.params || {};
    }

    async componentWillMount() {
        this.userId = await AsyncStorage.getItem('userId');

        this.setState({
            fromId: this.params?.fromId,
            fromLogin: this.params?.fromLogin,
            fromImage: this.params?.fromImage
        });
    }

    cancelCall = () => {
        this.props.navigation.pop();
    };

    answerCall = async () => {
        let {fromId} = this.state;

        let [error, response] = await FlaskAPI.post(`answer-call`, {
            receiver: this.userId,
            caller: fromId
        })

        if (error) {
            console.log(error);
        }

        let {sessionId, token} = response.data;

        this.props.navigation.navigate('CallRoom', {sessionId, token})
    };

    render() {
        let {fromImage, fromLogin} = this.state;
        return (
            <View style={{alignItems: 'center', flex: 1, backgroundColor: '#ffffff'}}>
                <View style={{alignItems: 'center', top: 50}}>
                    <Image source={{uri: fromImage}} style={{width: 200, height: 200}}/>
                    <Text style={{color: 'black'}}>{fromLogin}</Text>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center', top: 250}}>
                    <Button title="Accept" onPress={this.answerCall} />
                    <Button title="Cancel" onPress={this.cancelCall} />
                </View>
            </View>
        );
    }
}

export default CallReceive;