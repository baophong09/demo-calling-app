/**
 * @format
 */

import React from "react";
import {AppRegistry, YellowBox} from 'react-native';
import App from './src/index';
import {name as appName} from './app.json';
import {createAppContainer} from "react-navigation";



let AppContainer = createAppContainer(App);

const AppWrapper = () => (
    <AppContainer />
);

// Ignore specific yellowbox warnings
YellowBox.ignoreWarnings(["Require cycle:", "Remote debugger", "Setting a timer"]);

AppRegistry.registerComponent(appName, () => AppWrapper);
